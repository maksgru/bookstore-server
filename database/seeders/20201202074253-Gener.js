'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Geners', [
     {
       name: 'Fantasy',
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
      name: 'Textbook',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Classic',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Western',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Humor',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Horror',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Geners', null, {});
  }
};
